import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { searchForValue } from "../../store/Actions";

import "./Searchbar.scss";

const Searchbar = () => {
  const [searchedValue, setSearchedValue] = useState("");
  const initialState = useSelector((state) => state.tasks); // saving initila state 
  const dispatch = useDispatch();

  useEffect(
    () => dispatch(searchForValue(searchedValue, initialState)),
    [searchedValue]
  ); // checks if there is a change in searched value,
  // if so,filter the data again

  return (
    <div id="searchbar">
      <input
        type="text"
        placeholder="search"
        value={searchedValue}
        onChange={(e) => setSearchedValue(e.target.value)}
        onKeyDown={(e) => e.key === 8 && setSearchedValue(e.target.value)}
      ></input>
    </div>
  );
};

export default Searchbar;
