import {React,useState} from 'react'
import { useDispatch } from 'react-redux';
import {addNewTask} from '../../store/Actions'

import './AddTaskForm.scss'

const AddTaskForm = props => {

    const dispatch = useDispatch();

    const [newTaskTitle,setNewTaskTitle] = useState(); // creating a local state responsible for the new tasks title/value

    const addNewTaskAction = (e) => { // function invoked once a new value is to be added to the to-do list
        e.preventDefault();  // preventing defauld behaviour of the submit in form
        const regex = /^\s+$/gm; // regex to check if the input is just spaces
        if(!newTaskTitle || regex.test(newTaskTitle)){ //check if value is not correct
          return alert('Wrong Value')
        };
        dispatch(addNewTask({ //otherwhise dispach changes to the store => add new item
            id: props.nextIndex + 1,
            title: newTaskTitle,
            completed: false
        }))
        setNewTaskTitle('')
    }

    return (
        <form id="addTaskForm" onSubmit={e => addNewTaskAction(e)}>
            <input type='text' value={newTaskTitle} onChange={e => setNewTaskTitle(e.target.value)}/>
            <input type='submit' value='Add new Task'/>
        </form>
    )
}

export default AddTaskForm
