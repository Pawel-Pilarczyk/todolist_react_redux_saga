import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { removeTask, updateTask } from "../../store/Actions"; //action import

import "./Task.scss";
import { BsFillXCircleFill, BsCheck, BsTrash, BsPencil } from "react-icons/bs"; // import of Icons

const Task = (props) => {
  // task componnet to display and aedit pericular tasks

  const dispatch = useDispatch();
  //states to monitor changes on the task
  const [editMode, setEditMode] = useState(false); //swapping to edit mode
  const [taskTitle, setTaskTitle] = useState(props.task.title); // initial state for dispalying tasks title/value
  const [taskTitleEdited, setTaskTitleEdited] = useState(props.task.title); //state to control edited value of tasks title/value
  const [taskCompleted, setTaskCompleted] = useState(props.task.completed); // state to control if task is checked/finished

  const cancelTaskEdition = () => { //function to cancel editMode
    setEditMode(false);
    setTaskTitleEdited(taskTitle); // set up the edited value back to orginal before edition
  };

  document.addEventListener("keydown", (e) => { // if there editmode is on and escape is pressed, cancel task Edtion
    if (e.key === "Escape" && editMode) {
      cancelTaskEdition();
    }
  });

  const confirmTaskEdition = () => { // funcion to check if value that has been edited is now  valid 
    const regex = /^\s+$/gm;
    if (!taskTitleEdited || regex.test(taskTitleEdited)) {
      return alert("Wrong Value");
    }
    setEditMode(false);
    setTaskTitle(taskTitleEdited.trim());
  };

  useEffect(() => { //function to trigger update on of the state once value of taskTitle, taskCompleted is edited
    dispatch(
      updateTask({
        id: props.task.id,
        title: taskTitle,
        completed: taskCompleted,
      })
    );
  }, [taskTitle, taskCompleted]);

  useEffect(() => { // function to listen for updates on global state - works with SelectAllTasks 
    setTaskCompleted(props.task.completed);
  }, [props.task.completed]);

  return (
    <div className="task__container">
      {editMode === false ? ( // if Edit mode is false, display value
        <div className="taskDisplay__Container">
          <input
            type="checkbox"
            value={taskCompleted}
            checked={taskCompleted}
            onChange={(e) => setTaskCompleted(e.target.checked)}
          />
          <h1 className="taksTitle">{taskTitle}</h1>
          <BsPencil onClick={() => setEditMode(true)} />
        </div>
      ) : ( // if it is on make it editable
        <div className="taskEdition__Container">
          <input
            type="text"
            value={taskTitleEdited}
            onChange={(e) => setTaskTitleEdited(e.target.value)}
          />
          <BsCheck onClick={() => confirmTaskEdition()} />
          <BsFillXCircleFill onClick={() => cancelTaskEdition()} />
        </div>
      )}
      <button onClick={() => dispatch(removeTask(props.task.id))}> 
        <BsTrash />
      </button>
    </div>
  );
};

export default Task;
