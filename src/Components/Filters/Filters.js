import React from "react";
import {
  removeAllFinishedTasks,
  loadAll,
  filterAllFinishedTasks,
} from "../../store/Actions";
import { useDispatch, useSelector } from "react-redux";

import "./Filters.scss";

const Filters = () => {
  const dispatch = useDispatch();
  const tasks = useSelector((state) => state.tasks);

  return (
    <div id="filters">
      <h1>Filters</h1>
      <button onClick={() => dispatch(loadAll(tasks))}>All</button>
      <button onClick={() => dispatch(filterAllFinishedTasks())}>
        Completed
      </button>
      <button onClick={() => dispatch(removeAllFinishedTasks())}>
        Remove Completed Tasks
      </button>
    </div>
  );
};

export default Filters;
