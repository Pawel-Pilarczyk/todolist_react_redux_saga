import React from "react"; //imports 
import { useDispatch } from "react-redux";
import {
  markAllTaskAsCompleted,
  markAllTaskAsIncompleted,
} from "../../store/Actions";

import "./SelectAllTasks.scss";

const SelectAllTasks = () => { 
  const dispatch = useDispatch();

  const selectUnselectAll = e => { //function that toggles if the the components should be checked or unchecked
    e.target.checked
      ? dispatch(markAllTaskAsCompleted())
      : dispatch(markAllTaskAsIncompleted());
  };

  return (
    <div id="selectAll">
      <label for="markAllTasksCheckbox">Mark All Tasks</label>
      <input
        id="markAllTasksCheckbox"
        type="checkbox"
        onChange={(e) => selectUnselectAll(e)}
      />
    </div>
  );
};

export default SelectAllTasks;
