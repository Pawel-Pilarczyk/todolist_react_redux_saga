import React from "react";
import { useSelector } from "react-redux";

import Task from "../Task/Task";

import "./TasksContainer.scss";

const TaskContainer = () => {
  const tasksToBeDisplayes = useSelector(state => state.tasksToBeDisplayed); //receiveing data to be displayed

  const tasks = tasksToBeDisplayes.map((item) => ( //funciton to map through the items
    <Task key={item.id} task={item}></Task>
  ));

  return (
    <div id="tasks__contaner">{tasks.length ? tasks : <h1>No items</h1>}</div>
  );
};

export default TaskContainer;
