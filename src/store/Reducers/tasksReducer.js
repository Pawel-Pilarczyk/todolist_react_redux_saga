const tasksReducer = (state = [], action) => {
  let updatedTaskList;
  switch (action.type) {
    case "ADD_NEW_TASK":
      return [...state, action.payload];
    case "REMOVE_TASK":
      updatedTaskList = state.filter((item) => item.id !== action.payload);
      return updatedTaskList;
    case "UPDATE_TASK":
      updatedTaskList = state.filter((task) => task.id !== action.payload.id);
      updatedTaskList = [...updatedTaskList, action.payload];
      updatedTaskList = updatedTaskList.sort((a, b) => a.id - b.id);
      return updatedTaskList;
    case "REMOVE_ALL_FINISHED_TASKS":
      updatedTaskList = state.filter((task) => !task.completed);
      return updatedTaskList;
    case "MARK_ALL_TASKS_AS_COMPLETED":
      updatedTaskList = state.map(
        (task) => (task = { id: task.id, title: task.title, completed: true })
      );
      return updatedTaskList;
    case "MARK_ALL_TASKS_AS_INCOMPLETED":
      updatedTaskList = state.map(
        (task) => (task = { id: task.id, title: task.title, completed: false })
      );
      return updatedTaskList;
    case "SAVE_TO_MEMORY": // saving the state to local memory
      localStorage.setItem("toDoAppData", JSON.stringify(state));
      return state;
    case "LOAD_FROM_MEMORY":// loading the state from local memory
      return localStorage.getItem("toDoAppData")
        ? JSON.parse(localStorage.getItem("toDoAppData"))
        : [];
    default:
      return state; //
  }
};

export default tasksReducer;
