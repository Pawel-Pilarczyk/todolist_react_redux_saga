const tasksToBeDisplayedReducer = (state = [], action) => {
  switch (action.type) {
    case "LOAD_ALL":
      return [...action.payload];
    case "FILTER_ALL_FINISHED_TASKS":
      return state.filter((task) => task.completed);
    case "SEARCH_FOR_VALUE":
      return action.state.filter(task => task.title.includes(action.payload));
    default:
      return state;
  }
};

export default tasksToBeDisplayedReducer;
