export const addNewTask = (newTask) => ({
  type: "ADD_NEW_TASK",
  payload: newTask,
});

export const removeTask = (taskID) => ({
  type: "REMOVE_TASK",
  payload: taskID,
});

export const updateTask = (updatedTask) => ({
  type: "UPDATE_TASK",
  payload: updatedTask,
});

export const removeAllFinishedTasks = () => ({
  type: "REMOVE_ALL_FINISHED_TASKS",
  payload: "",
});

export const markAllTaskAsCompleted = () => ({
  type: "MARK_ALL_TASKS_AS_COMPLETED",
});

export const markAllTaskAsIncompleted = () => ({
  type: "MARK_ALL_TASKS_AS_INCOMPLETED",
});

export const filterAllFinishedTasks = () => ({
  type: "FILTER_ALL_FINISHED_TASKS",
});

export const loadAll = (payload) => ({ type: "LOAD_ALL", payload: payload });

export const searchForValue = (payload, state) => ({
  type: "SEARCH_FOR_VALUE",
  payload: payload,
  state: state,
});

export const saveToMemory = () => ({ type: "SAVE_TO_MEMORY" });

export const loadFromMemory = () => ({ type: "LOAD_FROM_MEMORY" });
