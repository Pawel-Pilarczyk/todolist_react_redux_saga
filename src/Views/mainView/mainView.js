import React, { useEffect } from "react"; //importing of Readt adn Redux parts
import { useSelector, useDispatch } from "react-redux";
import { loadAll, saveToMemory, loadFromMemory } from "../../store/Actions";

//import of components
import TaskContainer from "../../Components/TasksContainer/TasksContainer";
import AddTaskForm from "../../Components/AddTaskForm/AddTaskForm";
import Filters from "../../Components/Filters/Filters";
import SelectAllTasks from "../../Components/SelectAllTasks/SelectAllTasks";
import Searchbar from "../../Components/Searchbar/SearchBar";

import "./mainView.scss"; //import of styles

const MainView = () => {
  const tasks = useSelector((store) => store.tasks); // const which holds a reference to main stores state
  const dispatch = useDispatch();

  useEffect(() => {
    localStorage.getItem("toDoAppData") && dispatch(loadFromMemory());
  }, []); // once the component is loaded for the first time, fit checks the local memory for the data, if it is there load it to store

  useEffect(() => { // on every change of the main store, 
    dispatch(loadAll(tasks));  // state that holds the values to be displayed is updated
    dispatch(saveToMemory()); // all changes are saved into the memory - local storage
  }, [tasks]);

  return (
    <div id="mainView">
      <h1>To Do List</h1>
      <Filters />
      <SelectAllTasks />
      <Searchbar />
      <AddTaskForm nextIndex={tasks.length ? tasks[tasks.length - 1].id : 1} />
      <TaskContainer />
    </div>
  );
};

export default (MainView);
