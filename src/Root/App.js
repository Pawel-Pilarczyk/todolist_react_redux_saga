import { React } from "react";

import MainView from "../Views/mainView/mainView";

function App(props) {
  return (
    <div>
      <MainView />
    </div>
  );
}

export default App;
