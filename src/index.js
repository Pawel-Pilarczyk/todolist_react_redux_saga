import React from "react";
import ReactDOM from "react-dom";

//importing store components
import { Provider } from "react-redux";
import { createStore, combineReducers } from "redux";
import tasksReducer from "./store/Reducers/tasksReducer";
import tasksToBeDisplayedReducer from "./store/Reducers/taskToBeDisplayedReducer";

//importing components and styles
import App from "./Root/App.js";
import "./index.scss";

//creating combined reducer
const allReducers = combineReducers({
  tasks: tasksReducer,
  tasksToBeDisplayed: tasksToBeDisplayedReducer,
});

//creating of store
const store = createStore(
  allReducers /* preloadedState, */,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
